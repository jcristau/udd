#!/usr/bin/env python

"""
This script imports lintian run results into the database
See lintian.debian.org
"""

from .aux import quote
from .gatherer import gatherer
import re
import lzma
import sys


def get_gatherer(connection, config, source):
    return LintianGatherer(connection, config, source)


class LintianGatherer(gatherer):
    extension_to_package_type_map = {
        "dsc": "source",
        "deb": "binary",
        "udeb": "udeb",
        "changes": "changes",
        "buildinfo": "buildinfo",
    }

    excluded_tags = [
        "octal-permissions",
        "trimmed-deb822-field",
    ]

    def __init__(self, connection, config, source):
        gatherer.__init__(self, connection, config, source)
        self.assert_my_config('path', 'table')

    def run(self):
        my_config = self.my_config

        # making space for new data
        cur = self.cursor()

        cur.execute("DELETE FROM %s" % my_config["table"])

        cur.execute("""PREPARE lintian_insert
          AS INSERT INTO %s (package, package_type, package_version, package_arch, tag, tag_type, information)
          VALUES ($1, $2, $3, $4, $5, $6, $7)""" % (my_config['table']))

        entries = []

        basepath = os.path.join(my_config['path'], 'results')

        file_name_regex = re.compile(r'^([^_]+)_([^_]+)(?:_([^_]+))?\.([^.]+)$')

        for xzjsonpath in os.listdir(basepath):
            # print(xzjsonpath)
            # we are totally assuming the files are xz-compressed

            with open(os.path.join(basepath, xzjsonpath)) as f:
                # PY3: the open() should be 'rb'
                raw_data = lzma.decompress(f.read())
                if not raw_data:
                    continue
                try:
                    product = json.loads(raw_data)['product']
                    stdout = product['stdout']
                    groups = stdout['groups']
                except (ValueError, TypeError):
                    # print()
                    # sys.stdout.flush()
                    print("Failed to parse", xzjsonpath, file=sys.stderr)
                    # sys.stderr.flush()
                    # print()
                    continue

                for group in groups:
                    for input_file in group['input_files']:

                        basename = os.path.basename(input_file['path'])

                        match = file_name_regex.match(basename)
                        if not match:
                            print('Cannot parse file name %s' % basename, file=sys.stderr)
                            continue

                        (package_name, version, architecture, extension) = match.groups()

                        package_type = LintianGatherer.extension_to_package_type_map[extension]
                        if architecture is None:
                            architecture = 'source'

                        for hint in input_file['hints']:

                            if hint['tag'] in LintianGatherer.excluded_tags:
                                continue

                            visibility = hint['visibility']

                            # adjust to UDD wording
                            if visibility == 'info':
                                visibility = 'information'

                            if "experimental" in hint:
                                visibility = 'experimental'

                            if "override" in hint:
                                visibility = 'overridden'

                            entries.append((
                                package_name,
                                package_type,
                                version,
                                architecture,
                                hint['tag'],
                                visibility,
                                hint.get('context', ""),
                            ))
            # print('.', end='')
            # sys.stdout.flush()

        # print()
        cur.executemany("EXECUTE lintian_insert (%s, %s, %s, %s, %s, %s, %s)", entries)
        cur.execute("DEALLOCATE lintian_insert")
        cur.execute("ANALYZE %s" % my_config["table"])


if __name__ == '__main__':
    LintianGatherer().run()

# vim:set et tabstop=4:
