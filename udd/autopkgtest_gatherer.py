#!/usr/bin/env python

"""
This script imports screenshot autopkgtest results from https://ci.debian.net/data/status/
"""

from .aux import quote
from .gatherer import gatherer
import json
# import simplejson as json
from sys import stderr, exit
from os import path

from psycopg2 import IntegrityError, InternalError

suites        = ( 'stable',
                  'testing',
                  'unstable',
                )

architectures = ( 'amd64',
                  'arm64',
                  'ppc64el',
                )

valid_keys = ( 'run_id',
    #           'created_at',           # Paul Gevers: should be ignored
    #           'updated_at',           # Paul Gevers: should be ignored
               'suite',
               'arch',
               'package',               # ----> should be renamed to 'source'
               'version',
               'trigger',               # usually package.*version
               'status',
               'requestor',             # 'britney', 'debci' or e-mail
               'pin_packages',          # []
    #           'worker',               # Paul Gevers: should be ignored (is 'null' anyway)
               'date',
               'duration_seconds',
               'last_pass_date',
               'last_pass_version',
               'message',               # see below
               'previous_status',
    #           'duration_human',       # Paul Gevers: duration_seconds and duration_human feel double and the former is leaner for in a database
    #           'blame',                # Paul Gevers: should be ignored
             )

def get_gatherer(connection, config, source):
  return autopkgtest_gatherer(connection, config, source)

class autopkgtest_gatherer(gatherer):
  # Autopkgtest data from https://ci.debian.net/data/status/

  def __init__(self, connection, config, source):
    gatherer.__init__(self, connection, config, source)
    self.assert_my_config('table')
    my_config = self.my_config

    cur = self.cursor()
    query = "DELETE FROM %s" % my_config['table']
    cur.execute(query)
    query = """PREPARE autopkgtest_insert 
                   (int, text, text, text, text, text, text, text, timestamp,
                    int, timestamp, text, text, text)
                   AS INSERT INTO %s
                   (run_id, source, suite, architecture, version, trigger, status, requestor, autopkgtest_date,
                    duration_seconds, last_pass_date, last_pass_version, message, previous_status)
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)""" % (my_config['table'])
    cur.execute(query)

    pkg = None

  def run(self):
    my_config = self.my_config
    #start harassing the DB, preparing the final inserts and making place
    #for the new data:
    cur = self.cursor()

    all_test_data = []
    autopkgtest_data_dir = my_config['path']
    for suite in suites:
        for arch in architectures:
            autopkgtestfile = path.join(autopkgtest_data_dir, 'packages_' + suite + '_' + arch + '.json')
            with open(autopkgtestfile, 'r') as json_file:
                apt_data = json.load(json_file)
            for data in apt_data:
                record = {}
                for key in data:
                    if key in valid_keys:
                        if key == 'package':
                            record['source'] = data['package']
                        elif key == 'arch':
                            if data['arch'] != arch:
                                print("Something is wrong.  Reading file for architecture %s but found 'arch' = %s" % (arch, data['arch']), file=stderr)
                                exit(1)
                            record['architecture'] = arch
                        elif key == 'suite':
                            if data['suite'] != suite:
                                print("Something is wrong.  Reading file for suite %s but found 'suite' = %s" % (suite, data['suite']), file=stderr)
                                exit(1)
                            record['suite'] = suite
                        elif key == 'date':
                            record['autopkgtest_date'] = data['date']
                        elif key in ('run_id', 'duration_seconds') :
                            try:
                                record[key] = int(data[key])
                            except TypeError:
                                if key == 'duration_seconds':
                                    record['duration_seconds'] = None
                                else:
                                    print("No data for key %s in record %s" % (key, record), file=stderr)
                                    exit(1)
                        elif key in ('last_pass_date'):
                            if data['last_pass_date'] != 'never':
                                record['last_pass_date'] = data['last_pass_date']
                            else:
                                record['last_pass_date'] = None
                        else:
                            record[key] = data[key]
                        if data[key] == '':
                            if key in ('previous_status', 'last_pass_version', 'last_pass_date', 'trigger'):
                                record[key] = None
                            else:
                                print(("%s is empty in %s" % (key, data)))
                                exit(1)
                if 'suite' not in record:
                    record['suite'] = suite
                if 'architecture' not in record:
                    record['architecture'] = arch
                if 'requestor' not in record:
                    record['requestor'] = '' # FIXME: what's the role of this field?
                all_test_data.append(record)

    query = """EXECUTE autopkgtest_insert
                        (%(run_id)s, %(source)s, %(suite)s, %(architecture)s, %(version)s, %(trigger)s,
                         %(status)s, %(requestor)s, %(autopkgtest_date)s, %(duration_seconds)s, %(last_pass_date)s,
                         %(last_pass_version)s, %(message)s, %(previous_status)s)"""
    #print("DEBUG: " + query % all_test_data)
    try:
        cur.executemany(query, all_test_data)
        # cur.execute(query, all_test_data[1])
    except UnicodeEncodeError as err:
        print("Unable to inject autopkgtest data\n %s" % (err), file=stderr)
        print("-->", all_test_data, file=stderr)
    except KeyError as err:
        print("Unable to inject autopkgtest data\n %s" % (err), file=stderr)
    cur.execute("DEALLOCATE autopkgtest_insert")
    cur.execute("ANALYZE %s" % my_config['table'])

if __name__ == '__main__':
  main()

# vim:set et tabstop=2:

