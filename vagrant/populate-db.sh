#!/bin/sh

set -e
set -x

sshtarget="lucas@udd.debian.org"

if [ "$1" = "" ]; then
	echo "Specify target as parameter (schema, packages)"
	exit 1
fi

while [ "$1" != "" ]; do
	if [ "$1" = "all" ]; then
		# everything (except DD-restricted)
		dumptarget="-c --if-exists --exclude-table=ldap --exclude-table=pts"
	elif [ "$1" = "schema" ]; then
		# everything, without data (except DD-restricted)
		dumptarget="--schema-only -c --if-exists --exclude-table=ldap --exclude-table=pts"
	elif [ "$1" = "packages" ]; then
		# only tables related to sources/packages
		dumptarget="--data-only -t sources -t packages -t packages_summary -t uploaders"
	elif [ "$1" = "table" ]; then
		# only specified table
		dumptarget="--data-only -t $2"
		shift
	else
		echo "Unknown target: $1"
		exit 1
	fi
	shift

	fname="udd-dump-$(date +%s).$$.dump"
	ssh -t $sshtarget pg_dump --no-owner -p 5452 -Fc -v -f /tmp/$fname $dumptarget service=udd
	rsync -avP $sshtarget:/tmp/$fname /run/shm/$fname
	ssh $sshtarget rm -f /tmp/$fname
	pg_restore -U udd -j 8 -v -d udd /run/shm/$fname
done
