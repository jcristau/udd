#!/usr/bin/ruby
$LOAD_PATH.unshift File.dirname(__FILE__) + '/inc'
#STDERR.reopen(STDOUT) #makes live debugging easier
#puts "Content-type: text/plain\n\n"
require 'dmd-data'

now = Time::now
cgi = CGI::new
params = UDDData.parse_cgi_params(cgi.params)

if cgi.params != {}
  uddd = UDDData::new(
      params['emails'],
      params['packages'],
      params['bin2src'] == 'on',
      params['ignpackages'],
      params['ignbin2src'] == 'on',
      params['onlydev'],
      params['onlyrecent'],
      cgi.params['debug'][0] != nil)

  uddd.get_lintian(params)
else
  # default values
  params['lt_error'] = true
  params['lt_warning'] = true
end

format = cgi.params['format'][0]
page = Page.new(
    uddd ? uddd.lintian : nil,
    format,
    'templates/lintian.erb',
    {:params => params, :uddd => uddd, :tstart => now },
    'UDD - Lintian')
page.render
