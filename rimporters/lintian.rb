#!/usr/bin/ruby

$:.unshift File.dirname(__FILE__) + '/../rlibs'
$:.unshift File.dirname(__FILE__) + '/../rimporters'

# workers and slots
SLOTS_PER_WORKER = 12
WORKERS = {
  '3.68.246.71' => SLOTS_PER_WORKER,
#  '' => 12,
}
WORKER_HOST = WORKERS.keys.first
WORKER_LOGIN = 'root'
WORKER_KEY = "/srv/udd.debian.org/ssh/id_rsa_lintian_worker"
LINTIAN_WORKER_COMMAND = '/root/lintian-worker'
SOURCES_LIMIT_PER_RUN=9000

require 'udd-db'
require 'pp'
require 'net-ssh-exec3'
require 'pg'
require 'peach'
require 'thread'
require 'fileutils'
Thread::abort_on_exception = true

=begin
CREATE TYPE public.lintian_tagtype AS ENUM (
    'experimental',
    'overridden',
    'pedantic',
    'information',
    'warning',
    'error',
    'classification'
);

CREATE TABLE lintian_results (
lintian_version debversion not null,
source text not null,
version debversion not null,
package text,
package_version debversion,
architecture text,
tag_type lintian_tagtype,
tag text,
information text
);

CREATE TABLE lintian_logs (
  ts TIMESTAMP NOT NULL,
  lintian_version debversion not null,
  source text not null,
  version debversion not null,
  package text,
  package_version debversion,
  architecture text,
  result TEXT not null,
  duration integer NOT NULL,
  message TEXT
);

CREATE INDEX lintian_logs_idx ON lintian_logs USING btree (lintian_version, source, version, package, package_version, architecture);

CREATE TABLE lintian_results_agg (
source text not null,
version debversion not null,
package text,
package_version debversion,
architectures text[],
tag_type lintian_tagtype,
tag text,
information text,
lintian_version debversion not null,
count bigint);

CREATE INDEX lintian_results_agg_idx ON lintian_results_agg USING btree (source, version, package, package_version);

CREATE TABLE lintian (
package text not null,
tag_type lintian_tagtype,
package_type text,
version debversion,
architecture text,
tag text,
information text
);

CREATE INDEX lintian_idx ON lintiang USING btree (package);

=end

lintian_versions = []
WORKERS.each_pair do |k, _v|
  worker = Net::SSH::start(k, WORKER_LOGIN, :keys => [ WORKER_KEY ], :keys_only => true)
  lintian_version = worker.exec3!("lintian --version", :no_output => true, :no_log => true)[:stdout].chomp.split('v', 2)[1]
  puts "Lintian version on #{k}: #{lintian_version}"
  lintian_versions << lintian_version
  # clean up tmp files from previous runs
  worker.exec3!("rm -rf /tmp/lintian-pool-* /tmp/lw.*", :no_output => true, :no_log => true)
  # temporary hack
  worker.exec3!("rm -rf /tmp/mldbm-elf-[a-l]*", :no_output => true, :no_log => true)
  worker.exec3!("rm -rf /tmp/mldbm-elf-[m-z]*", :no_output => true, :no_log => true)
  worker.exec3!("rm -rf /tmp/mldbm-elf-[A-L]*", :no_output => true, :no_log => true)
  worker.exec3!("rm -rf /tmp/mldbm-elf-[M-Z]*", :no_output => true, :no_log => true)
  worker.exec3!("rm -rf /tmp/mldbm-elf-*", :no_output => true, :no_log => true)
  worker.shutdown!
end
if lintian_versions.uniq.length != 1
  raise "Non homogeneous lintian versions on workers"
end
$lintian_version = lintian_versions.first

$db = PG.connect({ :dbname => 'udd', :port => 5452})
$db.prepare('lr_insert', "INSERT INTO lintian_results (lintian_version, source, version, package, package_version, architecture, tag_type, tag, information)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)")
$db.prepare('lrlog_insert', "INSERT INTO lintian_logs (ts, lintian_version, source, version, package, package_version, architecture, result, duration, message)
      VALUES (NOW(), '#{$lintian_version}', $1, $2, $3, $4, $5, $6, $7, $8)")

def tagtype(t)
  t = t[0..0]
  return 'experimental' if t == 'X'
  return 'overridden' if t == 'O'
  return 'pedantic' if t == 'P'
  return 'information' if t == 'I'
  return 'warning' if t == 'W'
  return 'error' if t == 'E'
  return 'classification' if t == 'C'
  return 'masked' if t == 'M'
  raise "invalid tagtype: #{t}"
end

def lintian_log(source, version, package, package_version, architecture, result, duration, message)
  $db.exec_prepared('lrlog_insert', [ source, version, package, package_version, architecture, result, duration, message ])
end

def lintian_to_process
  ti = Time::now
  q = <<-EOF
  select distinct s.source, s.version as version, package, packages.version as package_version, packages.architecture as architecture, packages.filename, coalesce(dur.duration, 0) as duration
  from sources_uniq s
  left join packages on s.source = packages.source and s.version = packages.source_version and s.distribution = packages.distribution and s.release = packages.release
  left join (select distinct on (source) source, duration from lintian_logs order by source, duration desc) dur on s.source = dur.source
  where s.distribution = 'debian' and s.release in ('sid', 'experimental') and s.component = 'main'
  and (s.source, s.version) IN
  (
    (
    select l.source, l.version
    from sources_uniq as l
    where distribution = 'debian' and release in ('sid', 'experimental') and component = 'main'
    and NOT EXISTS (
      SELECT null
      FROM lintian_logs r
      WHERE lintian_version = '#{$lintian_version}'
      AND l.source = r.source and l.version = r.version
      AND result = 'ok'
    )
    )
    UNION
    (
    select p.source, p.source_version as version
    from packages p
    where distribution = 'debian' and release in ('sid', 'experimental') and component = 'main'
    and NOT EXISTS (
       SELECT null
       FROM lintian_logs lr2
       WHERE p.package = lr2.package AND p.version = lr2.package_version AND p.architecture = lr2.architecture
       AND lintian_version = '#{$lintian_version}'
       AND result = 'ok'
     )
     )
  )
  EOF
# and MD5(sources.source) ~ '^ab'
#  puts q

  r = $db.exec(q).to_a
  r2 = r.group_by { |e| [ e['source'], e['version'] ] }.to_a
  r2.map! do |e| 
    e[1].reject! { |f| f['package'].nil? } # if there are no binaries, the LEFT JOIN generates an empty result here
    if e[1].empty?
      dur = 0
    else 
       dur = e[1].first['duration'].to_i
    end
    e2 = { 'source' => e[0], 'binaries' => e[1], 'duration' => dur }
    e2
  end
  if $lintian_version == '2.115.3'
    r2.reject! { |e| e['source'] == 'unidic-mecab' } # #1021099
    r2.reject! { |e| e['source'] == 'latex-cjk-chinese-arphic' } # #1020930
  end
  tf = Time::now
  puts "#{Time::now} To process: #{r2.length} (will process at most #{SOURCES_LIMIT_PER_RUN}) (list generated in #{(tf-ti).to_i}s)"
  r2 = r2.sort { |a, b| a['duration'] <=> b['duration'] }
  # restrict to wanted number
  r2 = r2[0...SOURCES_LIMIT_PER_RUN]
  # reverse so that longest tasks come first
  return r2.reverse
end

def process_one(w, s)
  res = {}
  res[:stdout] = ""
  res[:stderr] = ""
  res[:exit_code] = nil
  res[:exit_signal] = nil
  ts = Time::now
  w.open_channel do |channel|
    channel.exec(LINTIAN_WORKER_COMMAND) do |_ch, success|
      unless success
        abort "FAILED: couldn't execute command (ssh.channel.exec)"
      end
      channel.on_data do |_ch,data|
        res[:stdout]+=data
      end

      channel.on_extended_data do |_ch,_type,data|
        res[:stderr]+=data
      end

      channel.on_request("exit-status") do |_ch,data|
        res[:exit_code] = data.read_long
      end

      channel.on_request("exit-signal") do |_ch, data|
        res[:exit_signal] = data.read_long
      end
      channel.send_data JSON::dump(s)
      channel.eof!
    end
  end
  w.loop
  str = "#{s['source'][0]}/#{s['source'][1]}"
  if res[:exit_code] != 0
    raise "SSH exec3 failed for #{str} (code #{res[:exit_code]} ; stderr: #{res[:stderr]})"
  end
  return [res[:stdout], res[:stderr]]
end

def import_one(s, data, stderr, duration)
  $db.exec("BEGIN")
  $db.exec("DELETE FROM lintian_results WHERE source = '#{s['source'][0]}' and version = '#{s['source'][1]}'")
  $db.exec("DELETE FROM lintian_logs WHERE source = '#{s['source'][0]}' and version = '#{s['source'][1]}'")
  d = JSON::parse(data)
  d['source_output'].each_line do |l|
    tagtype_short, _s, _t, tag, info = l.split(' ', 5)
    next if tagtype_short == "N:" # those are "notes" for overrides
    tt = tagtype(tagtype_short)
    next if tt == 'classification' and ['octal-permissions', 'trimmed-field', 'trimmed-deb822-field'].include?(tag)
    $db.exec_prepared('lr_insert', [
      $lintian_version,
      s['source'][0],
      s['source'][1],
      nil,
      nil,
      'source',
      tt,
      tag,
      info
    ])
  end
  lintian_log(s['source'][0], s['source'][1], nil, nil, nil, 'ok', duration, stderr)
  d['binaries_output'].each do |b|
    b['output'].each_line do |l|
      tagtype_short, _s, tag, info = l.split(' ', 5)
      next if tagtype_short == "N:" # those are "notes" for overrides
      tt = tagtype(tagtype_short)
      next if tt == 'classification' and ['octal-permissions', 'trimmed-field', 'trimmed-deb822-field'].include?(tag)
      $db.exec_prepared('lr_insert', [
        $lintian_version,
        s['source'][0],
        s['source'][1],
        b['pkg']['package'],
        b['pkg']['package_version'],
        b['pkg']['architecture'],
        tt,
        tag,
        info
      ])
    end
    lintian_log(s['source'][0], s['source'][1], b['pkg']['package'], b['pkg']['package_version'], b['pkg']['architecture'], 'ok', duration, '')
  end
  $db.exec("COMMIT")
end

def wrap_import
  t0 = Time::now
  $db.exec("BEGIN")
  $db.exec("DELETE FROM lintian_results lr
WHERE NOT EXISTS
 (
  SELECT null
  FROM sources_uniq s
  WHERE distribution = 'debian' and release in ('sid', 'experimental') and component = 'main'
  AND lr.source = s.source and lr.version = s.version
 )
OR (
  lr.package IS NOT NULL
  AND NOT EXISTS
 (
  SELECT NULL
  FROM packages p
  WHERE distribution = 'debian' and release in ('sid', 'experimental') and component = 'main'
  AND lr.package = p.package and lr.package_version = p.version and lr.architecture = p.architecture
 )
)
           ")
 $db.exec("DELETE FROM lintian_logs lr
WHERE NOT EXISTS
 (
  SELECT null
  FROM sources_uniq s
  WHERE distribution = 'debian' and release in ('sid', 'experimental') and component = 'main'
  AND lr.source = s.source and lr.version = s.version
 )
OR (
  lr.package is NOT NULL
  AND NOT EXISTS
   (
    SELECT NULL
    FROM packages p
    WHERE distribution = 'debian' and release in ('sid', 'experimental') and component = 'main'
    AND lr.package = p.package and lr.package_version = p.version and lr.architecture = p.architecture
   )
)
  ")
  $db.exec("COMMIT")
  tf = Time::now
  puts "Cleaned up obsolete records in #{(tf - t0).to_i}s"
  t0 = Time::now
  $db.exec("REFRESH MATERIALIZED VIEW CONCURRENTLY lintian_results_agg");
  $db.exec("REFRESH MATERIALIZED VIEW CONCURRENTLY lintian");
  tf = Time::now
  puts "Refreshed materialized views in #{(tf - t0).to_i}s"
  t0 = Time::now
  q = <<-EOF
WITH lintian_agg AS (
  select s.source, tag_type, count(distinct(tag, information)) as count
  from sources_uniq s
  join lintian_results_agg l on s.source = l.source and s.version = l.version
  where distribution='debian' and release='sid'
  group by s.source, tag_type
)
select source,
  coalesce(max(count) filter (WHERE tag_type = 'error'), 0) as errors,
  coalesce(max(count) filter (WHERE tag_type = 'warning'), 0) as warnings,
  coalesce(max(count) filter (WHERE tag_type = 'pedantic'), 0) as pedantics,
  coalesce(max(count) filter (WHERE tag_type = 'experimental'), 0) as experimentals,
  coalesce(max(count) filter (WHERE tag_type = 'overridden'), 0) as overriddens
from lintian_agg
group by source
order by source;
  EOF
  r = $db.exec(q).to_a
  File::open("/srv/udd.debian.org/udd/web/lintian-qa-list.txt.new", "w") do |fd|
    r.each do |d|
      fd.puts d.values.join(' ')
    end
  end
  FileUtils::mv("/srv/udd.debian.org/udd/web/lintian-qa-list.txt.new", "/srv/udd.debian.org/udd/web/lintian-qa-list.txt")
  tf = Time::now
  puts "Refreshed stats (lintian-qa-list.txt) in #{(tf - t0).to_i}s"
end

m = Mutex::new
tp = lintian_to_process
t0 = Time::now
changes = false

ths = []
WORKERS.each_pair do |host,slots|
  (1..slots).each do |slot|
    ths << Thread::new(host, slot) do |host, slot|
      loop do
        source = nil
        m.synchronize do
          if tp.length % 100 == 0 or (tp.length < 100 and tp.length % 10 == 0) or (tp.length < 10)
            puts "#{Time::now} Remaining: #{tp.length}"
          end
          source = tp.shift
        end
        break if source.nil?
        worker = Net::SSH::start(host, WORKER_LOGIN, :keys => [ WORKER_KEY ], :keys_only => true)
        t0p = Time::now
        begin
          stdout, stderr = process_one(worker, source)
        rescue Exception => e
          puts "FAILED, SKIPPING: #{e.message}"
          m.synchronize do
            lintian_log(source['source'][0], source['source'][1], nil, nil, nil, 'error', (Time::now - t0p).to_i, e.message)
          end
          next
        end
        worker.shutdown!
        m.synchronize do
          import_one(source, stdout, stderr, (Time::now - t0p).to_i)
          changes = true
        end
      end
    end
  end
end

thr_wrap = Thread::new do
  loop do
    if changes
      m.synchronize do
        wrap_import
        changes = false
      end
    end
    sleep 1800
  end
end

ths.each do |th|
  th.join
end

m.synchronize do
  thr_wrap.kill
end

tf = Time::now
puts "Processed packages in #{(tf - t0).to_i}s"
wrap_import


