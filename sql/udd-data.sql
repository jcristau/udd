--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-0+deb10u1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: bts_tags; Type: TABLE DATA; Schema: public; Owner: udd
--

COPY public.bts_tags (tag, tag_type) FROM stdin;
patch	
wontfix	
moreinfo	
unreproducible	
fixed	
help	
security	
upstream	
pending	
d-i	
confirmed	
ipv6	
lfs	
fixed-in-experimental	
fixed-upstream	
l10n	
newcomer	
potato	release
woody	release
sarge	release
sarge-ignore	
etch	release
etch-ignore	
lenny	release
lenny-ignore	
squeeze	release
squeeze-ignore	
wheezy	release
wheezy-ignore	
jessie	release
jessie-ignore	
stretch	release
stretch-ignore	
buster	release
buster-ignore	
sid	release
experimental	release
bullseye	release
bullseye-ignore	
bookworm	release
bookworm-ignore	
ftbfs	\N
a11y	\N
trixie	release
trixie-ignore	release
\.


--
-- Data for Name: releases; Type: TABLE DATA; Schema: public; Owner: udd
--

COPY public.releases (release, releasedate, role, releaseversion, distribution, sort) FROM stdin;
etch	2007-04-08		4.0	debian	400
etch-security	2007-04-08		4.0	debian	401
etch-proposed-updates	2007-04-08		4.0	debian	402
lenny	2009-02-14		5.0	debian	500
lenny-security	2009-02-14		5.0	debian	501
lenny-proposed-updates	2009-02-14		5.0	debian	502
squeeze-security	2011-02-06		6.0	debian	601
squeeze-proposed-updates	2011-02-06		6.0	debian	602
wheezy-security	2013-05-05		7.0	debian	701
wheezy-proposed-updates	2013-05-05		7.0	debian	702
sid	\N	unstable		debian	100000
experimental	\N	experimental		debian	0
squeeze	2011-02-06		6.0	debian	600
wheezy	2013-05-05		7.0	debian	700
stretch-security	2017-06-17		9	debian	901
stretch-proposed-updates	2017-06-17		9	debian	902
jessie-security	2015-04-26		8	debian	801
jessie-proposed-updates	2015-04-26		8	debian	802
buster-security	2019-07-06		10	debian	1001
buster-proposed-updates	2019-07-06		10	debian	1002
bullseye-updates	2021-08-14	\N	11	distribution	1105
buster-backports-sloppy	2021-08-14	\N	10	distribution	1005
buster	2019-07-06	oldstable	10	debian	1000
stretch	2017-06-17	oldoldstable	9	debian	900
jessie	2015-04-26	\N	8	debian	800
bookworm	\N	testing	\N	debian	1200
bookworm-security	\N	\N	\N	debian	1201
bookworm-proposed-updates	\N	\N	\N	debian	1202
bullseye-security	2021-08-14		11	debian	1101
bullseye-proposed-updates	2021-08-14		11	debian	1102
bullseye	2021-08-14	stable	11	debian	1100
bullseye-backports	2021-08-14	\N	11	debian	1103
buster-backports	2019-07-06	\N	10	debian	1003
buster-updates	2019-07-06	\N	10	debian	1004
stretch-backports	2017-06-17	\N	9	debian	903
stretch-backports-sloppy	2017-06-17	\N	9	debian	904
wheezy-backports	2013-05-05	\N	7.0	debian	703
wheezy-backports-sloppy	2013-05-05	\N	7.0	debian	704
wheezy-updates	2013-05-05	\N	7.0	debian	705
jessie-kfreebsd-security	2015-04-26	\N	8	debian	810
\.


--
-- PostgreSQL database dump complete
--

