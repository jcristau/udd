#!/usr/bin/perl -w

# calculate key packages
# based on ruby version by Lucas

use strict;
use warnings;

use DBI;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

my $EXC_SRC = [
];
my $INC_SRC = [
	'debian-installer',
	'piuparts',
	'debian-cd',
	'debian-installer-netboot-images',
	# there should probably be a better way to add a list of packages needed
	# for vital debian infrastructure, but add this for now
	'sbuild',
	#929214
	'debian-cloud-images',
];
my $POPCON_PERCENT = 100; # x% of submissions must have the package installed
my $do_deps = 1;
my $TESTING='';
my $testing_archs = {};

my $debug = 0;
$| = 1 if ($debug);
my $now = time;

my $dbh = DBI->connect("dbi:Pg:dbname=udd;port=5452;user=udd");

my $sthc = do_query($dbh,"select max(insts) from popcon");
my $pg = $sthc->fetchrow_hashref();
my $popcon_max = $pg->{"max"};
my $minpopcon = int($popcon_max * $POPCON_PERCENT/100);
$sthc = do_query($dbh,"select release_name('testing') as testing");
$pg = $sthc->fetchrow_hashref();
$TESTING = $pg->{"testing"};
$sthc = do_query($dbh,"select distinct architecture from packages where release='$TESTING';");
while (my $pg = $sthc->fetchrow_hashref()) {
	$testing_archs->{$pg->{"architecture"}} = 1;
}

# source filter for queries in the package table
my $pkgsrcfilter = " release='$TESTING' and source not in ('".join("','",@$EXC_SRC)."') ";
# source filter for queries in the source table
my $srcfilter = " release='$TESTING' and source not in ('".join("','",@$EXC_SRC)."') and extra_source_only is null ";

my $provides = get_provides($dbh);

my $notfound = {};

sub debug {
	my $msg = shift;
	print $msg if ($debug);
}

sub do_query {
	my $handle = shift;
	my $query = shift;

	print ((time-$now)." $query\n") if $debug;
	my $sthc = $handle->prepare($query);
	$sthc->execute();
	return $sthc;
}

sub addsources {
	my $handle = shift;
	my $query = shift;
	my $srcs = shift;
	my $origin = shift;
	my $sthc = do_query($handle,$query);
	while (my $source = $sthc->fetchrow()) {
		if (!defined($srcs->{$source})) {
			$srcs->{$source} = $origin;
		}
	}
}

sub is_testing_arch {
	my $archstring = shift;
	my $in_testing = 0;

	my @archs = split m/\s+/, $archstring;
	foreach my $arch (@archs) {
		if ($arch eq "any") {
			$in_testing = 1;
		} elsif ($arch eq "linux-any") {
			$in_testing = 1;
		} elsif ($testing_archs->{$arch}) {
			$in_testing = 1;
		} elsif ($arch =~ m/^linux-(.*)$/ && $testing_archs->{$1}) {
			$in_testing = 1;
		} elsif ($arch =~ m/^any-(.*)$/ && $testing_archs->{$1}) {
			$in_testing = 1;
		} elsif ($arch =~ m/^[!]linux-any/) {
		} elsif ($arch =~ m/^[!](.*)$/) {
			# in theory, all testing architectures could be blacklisted
			# individually, but we assume that, when there is a blacklist for
			# something else than linux-any, there will be a testing arch that
			# matches the non-blacklisted archs
			$in_testing = 1;
		}
	}

	return $in_testing;
}


sub get_depends {
	my $handle = shift;
	my $query = shift;
	my $pkgfield = shift;
	my $depfields = shift;
	my $pkgs = shift;
	my $sthc = do_query($handle,$query);
	while (my $row = $sthc->fetchrow_hashref()) {
		my $pkg = $row->{$pkgfield};
		for my $field (@$depfields) {
			my $type = $field;
			$type =~ s/_/-/g;
			my $deps = $row->{$field};
			next unless $deps;
			DEPS:
			foreach my $dep_alt (split(/\s*[,]\s*/,$deps)) {
				ALT:
				foreach my $dep (split(/\s*[|]\s*/,$dep_alt)) {
					my $d = $dep;
					# version
					$dep =~ s/\([^\)]*\)//og;
					# build profile
					$dep =~ s/<[^\>]*>//og;
					$dep =~ s/^\s*+//o;
					$dep =~ s/\s*+$//o;

					# architecture
					if ($dep =~ s/\s*\[([^\]]*)\]//og) {
						unless (is_testing_arch($1)) {
							#print "skip $pkg $type $dep $d not in testing\n";
							next ALT;
						}
					}

					# multiarch annotation
					$dep =~ s/:any//og;
					$dep =~ s/:native//og;

					# we try every alternative
					# if we find one, we add it and move on to the next
					# dependency, otherwise, we try the next alternative
					if (defined $pkgs->{$dep}) {
						next DEPS;
					} else {
						if (my $src = get_source($handle, $dep)) {
							my $info = "$pkg $type $dep";
							$pkgs->{$dep} = $info;
							next DEPS;
						} elsif (my $r = $provides->{$dep}) {
							my $src = $r->{"source"};
							my $ppkg = $r->{"package"};
							my $info = "$pkg $type $dep provided by $ppkg";
							$pkgs->{$ppkg} = $info;
							next DEPS;
						}
					}
				}
			}
		}
	}
}

sub get_source {
	my $handle = shift;
	my $pkg = shift;

	return undef if ($notfound->{$pkg});

	my $query = "select source from packages where ".
		" $pkgsrcfilter and ".
		" package = '$pkg'";
	my $sthc = do_query($handle,$query);
	if (my ($source) = $sthc->fetchrow_array()) {
		return $source;
	}
	$notfound->{$pkg} = 1;
	return undef;
}

sub get_provides {
	my $handle = shift;
	my $provides = {};

	my $query = "select ".
		"distinct on (provide) ".
			"packages.source, ".
			"packages.package, ".
			"s.provide, ".
			"popcon_src.insts ".
		"from ".
			"(select * from packages where $pkgsrcfilter ) as packages ".
		"left join ".
			"popcon_src ".
			"on packages.source=popcon_src.source, ".
			"regexp_split_to_table(provides, '[ ,]') s(provide) ".
		"where ".
			"s.provide ~ '^[a-z].*\$' ".
		"order by ".
			"provide, ".
			"insts desc ";

	my $sthc = do_query($handle,$query);
	while (my $row = $sthc->fetchrow_hashref()) {
		$provides->{$row->{"provide"}} = $row;
	}

	return $provides;
}

sub add_pkg_sources {
	my $handle = shift;
	my $srcs = shift;
	my $pkgs = shift;

	my $newsrcs = {};
	my $query = "select source,package from packages where ".
		" $pkgsrcfilter and ".
		" package in ('".join("','",keys %$pkgs)."')";
	my $sthc = do_query($handle,$query);
	while (my ($source,$package) = $sthc->fetchrow_array()) {
		next if defined($srcs->{$source});
		$srcs->{$source} = $pkgs->{$package};
		$newsrcs->{$source} = $pkgs->{$package};
	}
	return $newsrcs;
}


debug "# Popcon submissions: $popcon_max -- $POPCON_PERCENT% = $minpopcon\n";

my $srcs = {map { $_ => "manual" } @$INC_SRC};
debug "# building initial list\n";

addsources($dbh,"select distinct source from sources where
	$srcfilter and
	( source in (select source from packages where release='$TESTING' and priority in ('standard', 'important', 'required')));
	",$srcs,"priority");

addsources($dbh,"select distinct source from sources where
	$srcfilter and
	( source in (select source from popcon_src where insts >= $minpopcon))
	",$srcs,"popcon");

addsources($dbh,"select distinct source from sources where
	$srcfilter and
    ( source in (select source from packages where section='debian-installer' and release='$TESTING'))
	",$srcs,"d-i");


foreach my $remove (@$EXC_SRC) {
	delete $srcs->{$remove};
}

debug "# Initial list: ".join(" ",sort keys %$srcs)."\n";
debug "# Now recursively getting build-depends...\n";
my $round = 1;
my $newsrcs = $srcs;
my $pkgs = {};

while (1) {
	debug "# Round $round, #srcs = ".(scalar keys %$srcs)."\n";

	debug "# Getting build-depends for sources\n";
	get_depends($dbh,"select source,build_depends,build_depends_indep,build_depends_arch from sources
		where $srcfilter and source in ('".join("','",keys %$newsrcs)."')",
		"source",
		["build_depends","build_depends_indep","build_depends_arch"],
		$pkgs);

	debug "# Getting sources for build-depends\n";
	my $newsrcs_a = add_pkg_sources($dbh,$srcs,$pkgs);

	my $newsrcs_c = {};
	if ($do_deps) {
		$pkgs = {};
		debug "# Getting depends for sources\n";
		get_depends($dbh,"select package,depends,pre_depends from packages
			where $pkgsrcfilter and source in ('".join("','",keys %$newsrcs)."')",
			"package",
			["depends","pre_depends"],
			$pkgs);

		debug "# Getting sources for depends\n";
		$newsrcs_c = add_pkg_sources($dbh,$srcs,$pkgs);
	}

	$newsrcs = {%$newsrcs_a,%$newsrcs_c};

	debug "# Adding ".(scalar keys %$newsrcs)." source packages: ".join(" ",sort keys %$newsrcs)."\n";
	last unless scalar keys %$newsrcs;
	$round++;
	last if ($round > 20);
}

#print "sources:\n";
#print Dumper $srcs;

debug "# Final list of ".(scalar keys %$srcs)." key source packages:\n";
do_query($dbh,"DELETE FROM key_packages;") unless $debug;
my $insert_handle = $dbh->prepare("INSERT INTO key_packages ".
	"(source, reason) VALUES (\$1,\$2);");
foreach my $source (sort keys %$srcs) {
	my $reason = $srcs->{$source};
	debug "$source\t$reason\n";
	$insert_handle->execute($source,$reason) unless $debug;
}

do_query($dbh,"ANALYZE key_packages") unless $debug;


debug "# ".(scalar keys %$srcs)." key source packages\n";
