#!/bin/sh

set -e

LOCALDIR="/srv/udd.debian.org/mirrors/lintian"

rm --recursive --force "$LOCALDIR"
mkdir --parents "$LOCALDIR"

# 2021-02-22 switch to lintian.d.*net* as asked by lechner on #debian-qa
wget --quiet --ca-directory /etc/ssl/ca-debian https://lintian.debian.org/static/results.tar --output-document "$LOCALDIR/results.tar"

tar --directory "$LOCALDIR" --extract --file "$LOCALDIR/results.tar"
test -d "$LOCALDIR/results"
